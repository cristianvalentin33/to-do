//
//  FriendView.m
//  To-do
//
//  Created by LaboratoriOS Cronian Academy on 29/04/15.
//  Copyright (c) 2015 Samson Gabriel. All rights reserved.
//

#import "FriendView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ViewController.h"
#import "LoginViewController.h"
#import "FriendTo-do.h"
#import <QuartzCore/CALayer.h>
@interface FriendView ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation FriendView{
    NSArray *frdn;
    UITableView *tableView1;
    NSArray *poza;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //tableView1 = [[UITableView alloc] initWithFrame:CGRectMake( 0, 0, self.view.bounds.size.width,self.view.bounds.size.height ) style:UITableViewStyleGrouped];
    
   
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends?fields=name,picture" parameters:nil HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if(error)
         {
             
         }
         else{
      
         
         frdn=[result objectForKey:@"data"];
//         poza=result[@"picture"][@"data"][@"url"];
         NSLog(@"%@",result);
         NSLog(@"asdfghhh %@",poza);
            [self.Frendvc reloadData];
         }
         
         
         
     }];
    self.navigationItem.title=@"Prieteni";
       // self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"next" style:UIBarButtonItemStylePlain target:self action:@selector(logoutfb:)];
    
    self.Frendvc = [[UITableView alloc] initWithFrame:CGRectMake( 0, 0, self.view.bounds.size.width,self.view.bounds.size.height ) style:UITableViewStyleGrouped];
    [self.view addSubview:self.Frendvc];
    self.Frendvc.delegate = self;
    self.Frendvc.dataSource = self;
    [_Frendvc registerClass:[UITableViewCell class] forCellReuseIdentifier:@"FriendRow"];


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)logoutfb:(UIBarButtonItem *)sender
{
    
    

    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier =@"FriendRow";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath ];
    //UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"string"];
    NSString *mes=frdn[indexPath.row][@"name"];
    NSLog(@"asdf :%@",mes);
    cell.textLabel.text =mes;
    
    NSString *poz= frdn[indexPath.row][@"picture"][@"data"][@"url"];
    NSData *imge=[[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:poz]];
    UIImage *hs=[UIImage imageWithData:imge];
    //UIImageView *hs2=[[UIImageView alloc] initWithImage:hs];
    CALayer *cellImageLayer = cell.imageView.layer;
    [cellImageLayer setCornerRadius:hs.size.width/3];
    [cellImageLayer setMasksToBounds:YES];
   
    //cell.imageView.layer.cornerRadius=cell.imageView.frame.size.width/2;
    //cell.imageView.layer.masksToBounds=YES;
    cell.imageView.image=hs;

    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    FriendTo_do *ftd=[[FriendTo_do alloc]init];
    ftd.data = frdn[indexPath.row][@"id"];
    [self.navigationController pushViewController:ftd animated:YES];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return frdn.count;
}

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
