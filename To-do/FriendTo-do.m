//
//  FriendTo-do.m
//  To-do
//
//  Created by LaboratoriOS Cronian Academy on 30/04/15.
//  Copyright (c) 2015 Samson Gabriel. All rights reserved.
//

#import "FriendTo-do.h"
#import <Parse/Parse.h>
@interface FriendTo_do ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) NSArray *items;
@end

@implementation FriendTo_do
{
    UITableView *tableView1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",self.data);
    tableView1 = [[UITableView alloc] initWithFrame:CGRectMake( 0, 0, self.view.bounds.size.width,self.view.bounds.size.height ) style:UITableViewStyleGrouped];
    [self.view addSubview:tableView1];
    self.navigationItem.title=@"Lista Prietenului";
    tableView1.delegate = self;
    tableView1.dataSource = self;
    [tableView1 registerClass:[UITableViewCell class] forCellReuseIdentifier:@"rows"];
    PFQuery *qu =[PFQuery queryWithClassName:@"ite"];

    [qu whereKey:@"user" equalTo:self.data];
    [qu findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(error)
        {
        
            
        }
        else
        {
            
             self.items=objects;
            NSLog(@"%@",self.items);
            [tableView1 reloadData];
        }
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier =@"rows";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath ];
    NSString *mes=self.items[indexPath.row][@"nume"];
    NSLog(@"asdf :%@",mes);
    cell.textLabel.text =mes;
    
    if ([self.items[indexPath.row][@"completed"] boolValue]) {
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType=UITableViewCellAccessoryNone;
    }

    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
