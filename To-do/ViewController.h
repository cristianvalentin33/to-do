//
//  ViewController.h
//  To-do
//
//  Created by Samson Gabriel on 08/04/15.
//  Copyright (c) 2015 Samson Gabriel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end

