//
//  ViewController2.m
//  To-do
//
//  Created by LaboratoriOS Cronian Academy on 23/04/15.
//  Copyright (c) 2015 Samson Gabriel. All rights reserved.
//

#import "LoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor grayColor];
    UIButton *sari =[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sari addTarget:self action:@selector(gotomain) forControlEvents:UIControlEventTouchDown];
    [sari setTitle:@"Login Facebook" forState:UIControlStateNormal];
    sari.frame=CGRectMake(105.0, 300.0, 160.0, 40.0);
    //sari.frame=CGRectMake(0, 0, self.view.bounds.size.width,self.view.bounds.size.height);
    sari.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:sari];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)gotomain
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login setLoginBehavior:FBSDKLoginBehaviorSystemAccount];
   
    [login logOut];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"Could not login to Facebook. Error: %@",error.description);
            // Process error
        } else if (result.isCancelled) {
            NSLog(@"User cancelled Facebook Login");
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"public_profile"] && [result.grantedPermissions containsObject:@"email"] && [result.grantedPermissions containsObject:@"user_friends"]) {
                NSLog(@"User logged in with success");
                
                if ([FBSDKAccessToken currentAccessToken]) {
                    
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         
                         [self dismissViewControllerAnimated:YES completion:nil];
                     }];
                }
            }
        }
    }];
    }

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
