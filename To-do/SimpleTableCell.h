//
//  SimpleTableCell.h
//  To-do
//
//  Created by LaboratoriOS Cronian Academy on 30/04/15.
//  Copyright (c) 2015 Samson Gabriel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableCell : UIViewController
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *prepTimeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;
@end
