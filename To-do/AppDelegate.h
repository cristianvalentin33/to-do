//
//  AppDelegate.h
//  To-do
//
//  Created by Samson Gabriel on 08/04/15.
//  Copyright (c) 2015 Samson Gabriel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

