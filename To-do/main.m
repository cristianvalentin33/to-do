//
//  main.m
//  To-do
//
//  Created by Samson Gabriel on 08/04/15.
//  Copyright (c) 2015 Samson Gabriel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
